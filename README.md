# iOSWeatherTask - Zadanie rekrutacyjne

####Bazując na dostarczonym szkielecie, napisz prostą aplikację iOS do sprawdzania prognozy pogody z wykorzystaniem [Open Weather API](http://openweathermap.org/api) i języka Swift 5.

Aplikacja powinna składać się z 2 ekranów: **CitySearch** i **WeatherDetails**. 

##Do Twoich zadań należy:
- zaimplementować SAYT (search-as-you-type) miast w **CitySearch** wykorzystując załączoną bazę miast,
- zapisać listę ostatnio wyszkiwanych miast,
- pobrać dane pogodowe dla miasta z końcówki [api.openweathermap.org/data/2.5/weather](http://api.openweathermap.org/data/2.5/weather), **przy wykorzystaniu wyszukiwania po city ID**. Dokumentacja: [https://openweathermap.org/current](https://openweathermap.org/current),
- dopisać brakujący UI ekranu WeatherDetails,
- gotowe rozwiązanie dostarczyć w formie pull requestu.

##Szkielet zawiera:
- storyboard z gotowym UI ekranu CitySearch i **wydmuszką ekranu WeatherDetails (bez przejścia między nimi)**,
- **bazę danych citylist.sqlite3**, zawierająca wszystkie lokalizacje, dla których możemy sprawdzić pogodę korzystając z OpenWeatherApi. ([lista miast w JSON](http://bulk.openweathermap.org/sample/city.list.json.gz)),
- baza składa się z jednej tabeli: "cities". Tabela "cities" składa się z 2 kolum: "name" i "json". Kolumna "json" zawiera JSONa w postaci:

        {  
           "id":7530819,
           "name":"Rzeszów",
           "country":"PL",
           "coord":{  
              "lon":21.98848,
              "lat":50.005451
           }
        }

- klasę **SQLite** umożliwiającą dostanie uchwytu do bazy sqlite3,
- modele **City** i **Weather**, kompatybilne z informacjami zwracanymi ze wspomnianej końcówki API. Dodatkowo **City** jest kompatybilne z jsonem zapisanym w dołączonej bazie sqlite.

##Założenia biznesowe:

####Ekran Citi Search
- na pierwszym widoku **CitiSearch** będzie można podać miasto, dla którego chce się wyszukać prognozę pogody. Po wyborze miasta przechodzimy do ekranu **WeatherDetails**,
- przy wpisywaniu nazwy miasta w searchBar, z bazy danych powinny być zaciągane nazwy miast, które zawierają wpisany w searchBarze tekst (min. 3 znaki),
- gdy search bar jest pusty, wyświetlają się poprzednio wyszukiwane miasta,
- miasta, dla których zostało wykonane wyszukiwanie zapamiętujemy, w sposób umożliwiający ich odtworzenie po restarcie apki.

####Ekran WeatherDetails
- UI ekranu **WeatherDetails** zaprojektuj wg własnego pomysłu. Przedstaw w czytelnej postaci informacje o aktualnej pogdzie w danej lokalizacji,
- w zależności od akutalnie panującej temperatury w danej lokalizacji, kolor etykiety wyświetlającej temperaturę powinien się zmieniać wg zasad:
   - poniżej 10 stopni kolor niebieski,
   - między 10 a 20 stopni kolor czarny,
   - powyżej 20 kolor czerwony.

##Założenia techniczne:
- rozwiązanie powinno zostać dostarczone jako pull request do tego projektu w BitBucket,
- aplikacja powinna wspierać iPhone'a. Minimalna wersja systemu - iOS 12,
- dozwolone jest wykorzystanie dowolnych bibliotek. Można korzystać z dowolnego menadżera zależności,
- możesz dowolnie zmieniać strukturę plików/katalogów projektu wg własnego uznania,
- przy pisaniu aplikacji warto zwrócić uwagę na obecne w kodzie markery **"//TODO: "**,
- wszystkie elementy szkieletu zostały stworzone, aby przyspieszyć pracę. O ile dozwolone jest napisanie aplikacji od zera, to wskazane jest skorzystanie z dołączonych klas/rozwiązań.

##Sprawdzać będziemy:
- spełnienie wymagań biznesowych,
- znajomość Swifta i iOS SDK,
- jakość i czystość kodu,
- jakość UI. Aplikacja nie musi być piękna, ale powinna być estetyczna, ergonomiczna i wyglądać natywnie.
- strukturę projektu
import Foundation

struct Weather: Codable {
    let locationID: CityID
    let locationName: String

    enum CodingKeys: String, CodingKey {
        case locationID = "id"
        case locationName = "name"
        case mainConditions = "main"
    }

    let mainConditions: Conditions

    struct Conditions: Codable {
        let temp: Float
        let tempMin: Float
        let tempMax: Float
        let pressure: Float
        let humidity: Int

        var temperatureInCelcius: Float {
            return temp - 272.15
        }

        enum CodingKeys: String, CodingKey {
            case temp
            case tempMin = "temp_min"
            case tempMax = "temp_max"
            case pressure
            case humidity
        }
    }
}

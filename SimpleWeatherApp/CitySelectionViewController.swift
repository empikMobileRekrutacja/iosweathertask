import UIKit

class CitySelectionViewController: UIViewController {

    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView?.dataSource = citiesDataSource
            tableView?.delegate = self
        }
    }

    private let citiesDataSource = CitiesDataSource()
    private let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Weather App"

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Cities"
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
    }

}

// MARK: - UITableViewDelegate
extension CitySelectionViewController: UITableViewDelegate {

}

// MARK: - UISearchResultsUpdatingDelegate
// TODO: Implement d
extension CitySelectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate
// TODO: Implement conformance to regexp: a-zA-Z
extension CitySelectionViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
}
